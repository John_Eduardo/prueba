package com.stayrent.modelo;

import javax.persistence.*;

@Entity
@Table(name = "t003_ciudad")
public class Ciudad {
	
	public Ciudad(int id, String nombre, int id_departamento) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.id_departamento = id_departamento;
	}
	
	@Id
	@Column(name = "f003_id")
	private int id;
	
	@Column(name = "f003_nombre")
	private String nombre;
	
	@Column(name = "f003_id_departamento")
	private int	id_departamento;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}
	
	
	
	
}
