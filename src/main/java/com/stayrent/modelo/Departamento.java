package com.stayrent.modelo;

import javax.persistence.*;

@Entity
@Table(name = "t002_departamento")
public class Departamento {
	
	public Departamento(int id, int id_pais, String nombre) {
		super();
		this.id = id;
		this.id_pais = id_pais;
		this.nombre = nombre;
	}
	
	@Id
	@Column(name = "f002_id")
	private int	id;
	@Column(name = "f002_id_pais")
	private int	id_pais;
	@Column(name = "f002_nombre")
	private String nombre;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_pais() {
		return id_pais;
	}
	public void setId_pais(int id_pais) {
		this.id_pais = id_pais;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
