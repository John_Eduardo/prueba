package com.stayrent.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stayrent.modelo.Ciudad;
@Repository
public interface CiudadRepositorio extends JpaRepository<Ciudad, Integer> {

}
