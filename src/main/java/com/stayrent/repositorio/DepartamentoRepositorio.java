package com.stayrent.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stayrent.modelo.Departamento;
@Repository
public interface DepartamentoRepositorio extends JpaRepository<Departamento, Integer> {

}
