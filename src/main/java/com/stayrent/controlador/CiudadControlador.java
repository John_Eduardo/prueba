package com.stayrent.controlador;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.stayrent.modelo.Departamento;
import com.stayrent.servicio.DepartamentoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ciudad")
public class CiudadControlador {



    private CiudadServicio ciudadServicio;
    @Autowired
    public CiudadControlador(CiudadControlador ciudadServicio)
    {
        this.ciudadServicio = ciudadServicio;
    }

    @PostMapping(path = "/guardar")
    public Ciudad crearCiudad (@RequestBody Ciudad ciudad) {

        return ciudadServicio.Crear(ciudad);
    }

    @PutMapping(path = "/actualizar")
    public ResponseEntity<Ciudad>  actualizarCiudad (@RequestBody Ciudad ciudad) {

        Ciudad city = ciudadServicio.BuscarPorId(ciudad.getId());

        if(ciudad ==null) {
            return new ResponseEntity<Ciudad>(HttpStatus.BAD_REQUEST);
        }
        ciudadServicio.Actualizar(ciudad);

        return new ResponseEntity<Ciudad>(ciudad,HttpStatus.OK);

    }

    @DeleteMapping(path = "/eliminar")
    public ResponseEntity<Ciudad> EliminaCiudad (@RequestParam(name="id")int id)
    {
        try	{
            ciudadServicio.Eliminar(id);
            return new ResponseEntity<Ciudad>(HttpStatus.OK);

        }catch (Exception e) {
            return new ResponseEntity<Ciudad>(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(path = "/todos")
    public List<Ciudad> BuscarTodosCiudad()
    {return ciudadServicio.BuscarTodos();}

    @GetMapping(path = "/id")
    public ResponseEntity<Ciudad> BuscarPorId(@RequestParam(name = "id")int id)

    {
        Ciudad city = ciudadServicio.BuscarPorId(id);

        if(city ==null) {
            return new ResponseEntity<Ciudad>(city,HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Ciudad>(city,HttpStatus.OK);

    }




}
