package com.stayrent.controlador;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.stayrent.modelo.Departamento;
import com.stayrent.servicio.DepartamentoServicio;
import java.util.List;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/departamento")
public class DepartamentoControlador {
	


		private DepartamentoServicio departamentoServicio;
		@Autowired
		public DepartamentoControlador(DepartamentoServicio departamentoServicio) 
		{
				this.departamentoServicio = departamentoServicio;
		}

		@PostMapping(path = "/guardar")
		public Departamento crearDepto (@RequestBody Departamento depto) {
				
			return departamentoServicio.Crear(depto);
		}
		
		@PutMapping(path = "/actualizar")
		public ResponseEntity<Departamento>  actualizarDepto (@RequestBody Departamento depto) {
			 
			Departamento depa = departamentoServicio.BuscarPorId(depto.getId());
			
			if(depa ==null) {
				return new ResponseEntity<Departamento>(HttpStatus.BAD_REQUEST);
			}
			 departamentoServicio.Actualizar(depto);
			
			return new ResponseEntity<Departamento>(depto,HttpStatus.OK);

		}
		
		@DeleteMapping(path = "/eliminar")
		public ResponseEntity<Departamento> EliminaDepto (@RequestParam(name="id")int id)
		{
			try	{
			   departamentoServicio.Eliminar(id);
			   return new ResponseEntity<Departamento>(HttpStatus.OK);
			  
			}catch (Exception e) {
				 return new ResponseEntity<Departamento>(HttpStatus.NOT_FOUND);
			}
			
		}
		
		@GetMapping(path = "/todos")
		public List<Departamento> BuscarTodosDepto()
		{return departamentoServicio.BuscarTodos();}
		
		@GetMapping(path = "/id")
		public ResponseEntity<Departamento> BuscarPorId(@RequestParam(name = "id")int id)
		
		{
			Departamento depto = departamentoServicio.BuscarPorId(id);
			
			if(depto ==null) {
				return new ResponseEntity<Departamento>(depto,HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<Departamento>(depto,HttpStatus.OK);
			
		}
			
					
		
	
}
