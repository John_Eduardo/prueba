package com.stayrent.servicio;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.stayrent.modelo.Departamento;
import com.stayrent.repositorio.DepartamentoRepositorio;

@Service
public class DepartamentoServicio {

private final DepartamentoRepositorio DepaRepo;
	
	@Autowired
	public DepartamentoServicio(DepartamentoRepositorio DepaRepo) {
		
		this.DepaRepo = DepaRepo;
	}
	
	
	public Departamento Crear(Departamento departamento){ return DepaRepo.save(departamento); }
	
	public Departamento Actualizar (Departamento departamento)
	{
		return DepaRepo.save(departamento);
	}
	
	public void Eliminar (int id)
	{
		DepaRepo.deleteById(id);
	}
	
	public List<Departamento> BuscarTodos()
	{
		return DepaRepo.findAll();
	}
	
	public Departamento BuscarPorId(Integer Id)
	{
	 return DepaRepo.getOne(Id);	
	}
}
