package com.stayrent.servicio;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stayrent.modelo.Ciudad;
import com.stayrent.repositorio.CiudadRepositorio;

@Service
public class CiudadServicio {

	private final CiudadRepositorio ciudadRepo;
	
	@Autowired
	public CiudadServicio(CiudadRepositorio ciudadRepo) {
		
		this.ciudadRepo = ciudadRepo;
	}
	
	
	public Ciudad Crear(Ciudad ciudad)
	{
		return ciudadRepo.save(ciudad);
	}
	
	public Ciudad Actualizar (Ciudad ciudad)
	{
		return ciudadRepo.save(ciudad);
	}
	
	public void Eliminar (int id)
	{
		ciudadRepo.deleteById(id);
	}
	
	public List<Ciudad> BuscarTodos()
	{
		return ciudadRepo.findAll();
	}
	
	public Optional<Ciudad> BuscarPorId(Integer Id)
	{
	 return ciudadRepo.findById(Id);	
	}
}
