SET IDENTITY_INSERT dbo.t002_departamento ON;
GO
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('1', 1,'AMAZONAS');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('2', 1,'ANTIOQUIA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('3', 1,'ARAUCA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('4', 1,'ATL�NTICO');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('5', 1,'BOL�VAR');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('6', 1,'BOYAC�');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('7', 1,'CALDAS');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('8', 1,'CAQUET�');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('9', 1,'CASANARE');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('10',1, 'CAUCA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('11',1, 'CESAR');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('12',1, 'CHOC�');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('13',1, 'C�RDOBA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('14',1, 'CUNDINAMARCA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('15',1, 'GUAIN�A');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('16',1, 'GUAVIARE');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('17',1, 'HUILA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('18',1, 'LA GUAJIRA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('19',1, 'MAGDALENA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('20',1, 'META');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('21',1, 'NARI�O');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('22',1, 'NORTE DE SANTANDER');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('23',1, 'PUTUMAYO');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('24',1, 'QUIND�O');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('25',1, 'RISARALDA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('26',1, 'SAN ANDR�S Y ROVIDENCIA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('27',1, 'SANTANDER');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('28',1, 'SUCRE');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('32',1, 'VICHADA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('29',1, 'TOLIMA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('30',1, 'VALLE DEL CAUCA');
INSERT INTO t002_departamento (f002_id,f002_id_pais, f002_nombre) VALUES ('31',1, 'VAUP�S');

GO
-- SET IDENTITY_INSERT to ON.
SET IDENTITY_INSERT dbo.t002_departamento OFF;
GO
