
-- Elimina la Base de datos 
	--Ejecutar esta linea Inicialmente para  crear la base de datos, luego comentarla nuevamente
	-- CREATE DATABASE StayRentSoft;

	-- Usa la Base de datos que acabamos de crear para ejecutar el script
	USE StayRentSoft 

	-- Valida si hay llaves foraneas para eliminarlas y no generar conflicto
 IF (OBJECT_ID('dbo.f002_id_pais_FK', 'F') IS NOT NULL)
	BEGIN
		ALTER TABLE t002_departamento DROP CONSTRAINT f002_id_pais_FK;
		ALTER TABLE t003_ciudad DROP CONSTRAINT f003_id_departamento_FK;
		ALTER TABLE t004_barrio DROP CONSTRAINT f004_id_ciudad_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_barrio_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_tipo_docto_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_ocupacion_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_naturaleza_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_tipo_persona_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_estrato_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_genero_FK;
		ALTER TABLE t012_persona DROP CONSTRAINT f012_id_estado_civil_FK;
		ALTER TABLE t014_establecimiento DROP CONSTRAINT f014_id_persona_FK;
		ALTER TABLE t014_establecimiento DROP CONSTRAINT f014_id_parqueadero_FK;
		ALTER TABLE t014_establecimiento DROP CONSTRAINT f014_id_disponibilidad_FK;
		ALTER TABLE t017_reserva DROP CONSTRAINT f017_id_establecimiento_FK;
		ALTER TABLE t017_reserva DROP CONSTRAINT f017_id_estado_aprob_FK;
		ALTER TABLE t017_reserva DROP CONSTRAINT f017_id_propietario_FK;
		ALTER TABLE t017_reserva DROP CONSTRAINT f017_id_inquilino_FK;
		ALTER TABLE t019_alquiler DROP CONSTRAINT f019_id_reserva_FK;
		ALTER TABLE t019_alquiler DROP CONSTRAINT f019_id_calificacion_FK;
		ALTER TABLE t020_observaciones DROP CONSTRAINT f020_id_alquiler_FK;
	END

	--Elimina las tablas de la Base de datos
	DROP TABLE IF EXISTS dbo.t001_pais, dbo.t002_departamento, dbo.t003_ciudad, dbo.t004_barrio, dbo.t005_tipo_documento,
		dbo.t006_ocupacion, dbo.t007_naturaleza, t008_tipo_persona, dbo.t008_tipo_persona, dbo.t009_estrato, dbo.t010_genero,
		dbo.t011_estado_civil, dbo.t012_persona, dbo.t013_parqueadero, dbo.t014_establecimiento, dbo.t015_disponibilidad,
		dbo.t016_estado_aprobacion, dbo.t017_reserva, dbo.t018_calificacion, dbo.t019_alquiler, dbo.t020_observaciones;

	-- Crea la base de datos y las tabla
	CREATE TABLE t001_pais
	(f001_id INTEGER IDENTITY(1,1) NOT NULL,
	f001_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t002_departamento
	(f002_id INTEGER IDENTITY(1,1) NOT NULL,
	f002_id_pais INTEGER NOT NULL,
	f002_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t003_ciudad
	(f003_id INTEGER IDENTITY(1,1) NOT NULL,
	f003_id_departamento INTEGER NOT NULL,
	f003_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t004_barrio
	(f004_id INTEGER IDENTITY(1,1) NOT NULL,
	f004_id_ciudad INTEGER NOT NULL,
	f004_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t005_tipo_documento
	(f005_id INTEGER IDENTITY(1,1) NOT NULL,
	f005_nombre_docto VARCHAR(40) NOT NULL);
	
	CREATE TABLE t006_ocupacion
	(f006_id INTEGER IDENTITY(1,1) NOT NULL,
	f006_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t007_naturaleza
	(f007_id INTEGER IDENTITY(1,1) NOT NULL,
	f007_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t008_tipo_persona
	(f008_id INTEGER IDENTITY(1,1) NOT NULL,
	f008_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t009_estrato
	(f009_id INTEGER IDENTITY(1,1) NOT NULL,
	f009_referencia VARCHAR(40) NOT NULL);
	
	CREATE TABLE t010_genero
	(f010_id INTEGER IDENTITY(1,1) NOT NULL,
	f010_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t011_estado_civil
	(f011_id INTEGER IDENTITY(1,1) NOT NULL,
	f011_nombre VARCHAR(40) NOT NULL);
	
	CREATE TABLE t012_persona
	(f012_id INTEGER IDENTITY(1,1) NOT NULL,
	f012_id_barrio INTEGER NOT NULL,
	f012_id_tipo_docto INTEGER NOT NULL,
	f012_id_ocupacion INTEGER NOT NULL,
	f012_id_naturaleza INTEGER NOT NULL,
	f012_id_tipo_persona INTEGER NOT NULL,
	f012_id_estrato INTEGER NOT NULL,
	f012_id_genero INTEGER NOT NULL,
	f012_id_estado_civil INTEGER NOT NULL,
	f012_nombre VARCHAR(40) NOT NULL,
	f012_apellido VARCHAR(40) NOT NULL,
	f012_nro_identificacion INTEGER NOT NULL,
	f012_telefono1 VARCHAR(20) NOT NULL,
	f012_telefono2 VARCHAR(20) NOT NULL,
	f012_foto image,
	f012_email VARCHAR(80) NOT NULL,
	f012_password VARCHAR(11) NOT NULL);
	
	CREATE TABLE t013_parqueadero
	(f013_id INTEGER IDENTITY(1,1) NOT NULL,
	f013_caspacidad VARCHAR(40) NOT NULL);
	
	CREATE TABLE t014_establecimiento
	(f014_id INTEGER IDENTITY(1,1) NOT NULL,
	f014_id_persona INTEGER NOT NULL,
	f014_id_parqueadero INTEGER NOT NULL,
	f014_id_disponibilidad INTEGER NOT NULL,
	f014_direccion VARCHAR(90) NOT NULL,
	f014_desc_general VARCHAR(2000) NOT NULL,
	f014_capacidad VARCHAR(20) NOT NULL,
	f014_ingreso_animales VARCHAR(20) NOT NULL,
	f014_internet VARCHAR(20) NOT NULL,
	f014_valneario VARCHAR(20) NOT NULL,
	f014_cancha VARCHAR(20) NOT NULL,
	f014_calificacion_global VARCHAR(20) NOT NULL,
	f014_foto image,
	f014_precio INTEGER NOT NULL,
	f014_deposito INTEGER NOT NULL);
	
	CREATE TABLE t015_disponibilidad
	(f015_id INTEGER IDENTITY(1,1) NOT NULL,
	f015_estado VARCHAR(20) NOT NULL);
	
	CREATE TABLE t016_estado_aprobacion
	(f016_id INTEGER IDENTITY(1,1) NOT NULL,
	f016_estado_aprob VARCHAR(20) NOT NULL);
	
	CREATE TABLE t017_reserva
	(f017_id INTEGER IDENTITY(1,1) NOT NULL,
	f017_id_establecimiento INTEGER NOT NULL,
	f017_id_estado_aprob INTEGER NOT NULL,
	f017_id_propietario INTEGER NOT NULL,
	f017_id_inquilino INTEGER NOT NULL,
	f017_fecha_inicio date NOT NULL,
	f017_fecha_fin date NOT NULL);
	
	CREATE TABLE t018_calificacion
	(f018_id INTEGER IDENTITY(1,1) NOT NULL,
	f018_descripcion VARCHAR(20) NOT NULL);
	
	CREATE TABLE t019_alquiler
	(f019_id INTEGER IDENTITY(1,1) NOT NULL,
	f019_id_reserva INTEGER NOT NULL,
	f019_id_calificacion INTEGER NOT NULL,
	f019_estado VARCHAR(20) NOT NULL);
	
	CREATE TABLE t020_observaciones
	(f020_id INTEGER IDENTITY(1,1) NOT NULL,
	f020_id_alquiler INTEGER NOT NULL,
	f020_comentario VARCHAR(2000) NOT NULL);
	
	-- Crea las llaves Primarias PK de las tablas
	ALTER TABLE t001_pais ADD CONSTRAINT f001_id PRIMARY KEY(f001_id);
	ALTER TABLE t002_departamento ADD CONSTRAINT f002_id PRIMARY KEY(f002_id);
	ALTER TABLE t003_ciudad ADD CONSTRAINT f003_id PRIMARY KEY(f003_id);
	ALTER TABLE t004_barrio ADD CONSTRAINT f004_id PRIMARY KEY(f004_id);
	ALTER TABLE t005_tipo_documento ADD CONSTRAINT f005_id PRIMARY KEY(f005_id);
	ALTER TABLE t006_ocupacion ADD CONSTRAINT f006_id PRIMARY KEY(f006_id);
	ALTER TABLE t007_naturaleza ADD CONSTRAINT f007_id PRIMARY KEY(f007_id);
	ALTER TABLE t008_tipo_persona ADD CONSTRAINT f008_id PRIMARY KEY(f008_id);
	ALTER TABLE t009_estrato ADD CONSTRAINT f009_id PRIMARY KEY(f009_id);
	ALTER TABLE t010_genero ADD CONSTRAINT f010_id PRIMARY KEY(f010_id);
	ALTER TABLE t011_estado_civil ADD CONSTRAINT f011_id PRIMARY KEY(f011_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id PRIMARY KEY(f012_id);
	ALTER TABLE t013_parqueadero ADD CONSTRAINT f013_id PRIMARY KEY(f013_id);
	ALTER TABLE t014_establecimiento ADD CONSTRAINT f014_id PRIMARY KEY(f014_id);
	ALTER TABLE t015_disponibilidad ADD CONSTRAINT f015_id PRIMARY KEY(f015_id);
	ALTER TABLE t016_estado_aprobacion ADD CONSTRAINT f016_id PRIMARY KEY(f016_id);
	ALTER TABLE t017_reserva ADD CONSTRAINT f017_id PRIMARY KEY(f017_id);
	ALTER TABLE t018_calificacion ADD CONSTRAINT f018_id PRIMARY KEY(f018_id);
	ALTER TABLE t019_alquiler ADD CONSTRAINT f019_id PRIMARY KEY(f019_id);
	ALTER TABLE t020_observaciones ADD CONSTRAINT f020_id PRIMARY KEY(f020_id);
		
	-- Crea las llaves Foraneas FK de las tablas	
	ALTER TABLE t002_departamento ADD CONSTRAINT f002_id_pais_FK FOREIGN KEY(f002_id_pais) REFERENCES t001_pais(f001_id);
	ALTER TABLE t003_ciudad ADD CONSTRAINT f003_id_departamento_FK FOREIGN KEY(f003_id_departamento) REFERENCES t002_departamento(f002_id);
	ALTER TABLE t004_barrio ADD CONSTRAINT f004_id_ciudad_FK FOREIGN KEY(f004_id_ciudad) REFERENCES t003_ciudad(f003_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_barrio_FK FOREIGN KEY(f012_id_barrio) REFERENCES t004_barrio(f004_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_tipo_docto_FK FOREIGN KEY(f012_id_tipo_docto) REFERENCES t005_tipo_documento(f005_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_ocupacion_FK FOREIGN KEY(f012_id_ocupacion) REFERENCES t006_ocupacion(f006_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_naturaleza_FK FOREIGN KEY(f012_id_naturaleza) REFERENCES t007_naturaleza(f007_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_tipo_persona_FK FOREIGN KEY(f012_id_tipo_persona) REFERENCES t008_tipo_persona(f008_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_estrato_FK FOREIGN KEY(f012_id_estrato) REFERENCES t009_estrato(f009_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_genero_FK FOREIGN KEY(f012_id_genero) REFERENCES t010_genero(f010_id);
	ALTER TABLE t012_persona ADD CONSTRAINT f012_id_estado_civil_FK FOREIGN KEY(f012_id_estado_civil) REFERENCES t011_estado_civil(f011_id);
	ALTER TABLE t014_establecimiento ADD CONSTRAINT f014_id_persona_FK FOREIGN KEY(f014_id_persona) REFERENCES t012_persona(f012_id);
	ALTER TABLE t014_establecimiento ADD CONSTRAINT f014_id_parqueadero_FK FOREIGN KEY(f014_id_parqueadero) REFERENCES t013_parqueadero(f013_id);
	ALTER TABLE t014_establecimiento ADD CONSTRAINT f014_id_disponibilidad_FK FOREIGN KEY(f014_id_disponibilidad) REFERENCES t015_disponibilidad(f015_id);
	ALTER TABLE t017_reserva ADD CONSTRAINT f017_id_establecimiento_FK FOREIGN KEY(f017_id_establecimiento) REFERENCES t014_establecimiento(f014_id);
	ALTER TABLE t017_reserva ADD CONSTRAINT f017_id_estado_aprob_FK FOREIGN KEY(f017_id_estado_aprob) REFERENCES t016_estado_aprobacion(f016_id);
	ALTER TABLE t017_reserva ADD CONSTRAINT f017_id_propietario_FK FOREIGN KEY(f017_id_propietario) REFERENCES t012_persona(f012_id);
	ALTER TABLE t017_reserva ADD CONSTRAINT f017_id_inquilino_FK FOREIGN KEY(f017_id_inquilino) REFERENCES t012_persona(f012_id);
	ALTER TABLE t019_alquiler ADD CONSTRAINT f019_id_reserva_FK FOREIGN KEY(f019_id_reserva) REFERENCES t017_reserva(f017_id);
	ALTER TABLE t019_alquiler ADD CONSTRAINT f019_id_calificacion_FK FOREIGN KEY(f019_id_calificacion) REFERENCES t018_calificacion(f018_id);
	ALTER TABLE t020_observaciones ADD CONSTRAINT f020_id_alquiler_FK FOREIGN KEY(f020_id_alquiler) REFERENCES t019_alquiler(f019_id);



